﻿## jpegslide

JPEG をロスレススライド変換するコマンドラインツールです。

モザイク状に並べ替えられた JPEG のブロックを指定した位置にスライド移動し、ロスレス保存します。各ブロックは 8x8 の倍数である必要があります。

## 使い方

`jpegslide -i [input.jpg] -t [transform.json] -o [output.jpg] -tr [trim_px] -tb [trim_px]`

- `-i` - 入力 JPEG ファイルのパス
- `-t` - スライド定義 JSON ファイルのパス
- `-o` - 出力 JPEG ファイルのパス
- `-tr` - (オプション) 右トリミングサイズ (pixel)
- `-tb` - (オプション) 下トリミングサイズ (pixel)

**例**

`jpegslide -i sample.jpg -t sample.json -o output.jpg -tr 6`




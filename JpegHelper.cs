﻿using System.Linq;
using System.Reflection;
using BitMiracle.LibJpeg.Classic;

namespace jpegslide
{
    public static class JpegHelper
    {
        /// <summary>
        /// 垂直方向のブロック数を取得します。(通常 画像高さ / 8)
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static int Height_in_blocks(this jpeg_component_info info)
        {
            var propertyInfo = typeof(jpeg_component_info).GetField("height_in_blocks", BindingFlags.Instance | BindingFlags.NonPublic);
            return (int)propertyInfo.GetValue(info);
        }

        /// <summary>
        /// MCU サンプル幅を取得します。(通常 8)
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static int MCU_sample_width(this jpeg_component_info info)
        {
            var propertyInfo = typeof(jpeg_component_info).GetField("MCU_sample_width", BindingFlags.Instance | BindingFlags.NonPublic);
            return (int)propertyInfo.GetValue(info);
        }

        /// <summary>
        /// jvirt_array<JBLOCK> の JBLOCK[][] を取得します。
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static JBLOCK[][] GetBuffer(jvirt_array<JBLOCK> arr)
        {
            var propertyInfo = typeof(jvirt_array<JBLOCK>).GetField("m_buffer", BindingFlags.Instance | BindingFlags.NonPublic);
            return (JBLOCK[][])propertyInfo?.GetValue(arr);
        }

        /// <summary>
        /// jvirt_array<JBLOCK> の JBLOCK[][] をクローンします。
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static JBLOCK[][] CloneArray(this jvirt_array<JBLOCK> arr)
        {
            var buffer = GetBuffer(arr);
            var result = new JBLOCK[buffer.Length][];

            for (var i = 0; i < buffer.Length; i++)
            {
                result[i] = buffer[i].Clone() as JBLOCK[];
            }
            return result;
        }

        ///// <summary>
        ///// JPEG 出力時の幅をセットします。(単位: pixel)
        ///// </summary>
        ///// <param name="arr"></param>
        ///// <param name="output_width"></param>
        //public static void SetOutputWidth(this jvirt_array<JBLOCK> arr, int output_width)
        //{
        //    var decompress = arr.ErrorProcessor as jpeg_decompress_struct;
        //    var propertyInfo = typeof(jpeg_decompress_struct).GetField("m_output_width", BindingFlags.Instance | BindingFlags.NonPublic);
        //    propertyInfo?.SetValue(decompress, output_width);
        //}

        ///// <summary>
        ///// JPEG 出力時の高さをセットします。(単位: pixel)
        ///// </summary>
        ///// <param name="arr"></param>
        ///// <param name="output_width"></param>
        //public static void SetOutputHeight(this jvirt_array<JBLOCK> arr, int output_width)
        //{
        //    var decompress = arr.ErrorProcessor as jpeg_decompress_struct;
        //    var propertyInfo = typeof(jpeg_decompress_struct).GetField("m_output_height", BindingFlags.Instance | BindingFlags.NonPublic);
        //    propertyInfo?.SetValue(decompress, output_width);
        //}


        /// <summary>
        /// JPEG 出力時の幅をセットします。(単位: pixel)
        /// </summary>
        /// <param name="decompress">jpeg_decompress_struct</param>
        /// <param name="output_width">出力時の幅 (pixel)</param>
        public static void SetOutputWidth(this jpeg_decompress_struct decompress, int output_width)
        {
            var propertyInfo = typeof(jpeg_decompress_struct).GetField("m_output_width", BindingFlags.Instance | BindingFlags.NonPublic);
            propertyInfo?.SetValue(decompress, output_width);
        }

        /// <summary>
        /// JPEG 出力時の高さをセットします。(単位: pixel)
        /// </summary>
        /// <param name="decompress">jpeg_decompress_struct</param>
        /// <param name="output_height">出力時の高さ (pixel)</param>
        public static void SetOutputHeight(this jpeg_decompress_struct decompress, int output_height)
        {
            var propertyInfo = typeof(jpeg_decompress_struct).GetField("m_output_height", BindingFlags.Instance | BindingFlags.NonPublic);
            propertyInfo?.SetValue(decompress, output_height);
        }

    }
}
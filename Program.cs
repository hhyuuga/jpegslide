﻿using BitMiracle.LibJpeg.Classic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Reflection;
using System.Text.RegularExpressions;

namespace jpegslide
{
	class Program
	{
		static void Main(string[] args)
		{
            Console.OutputEncoding = Encoding.UTF8;

            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

            var options = ParseCommandLine();

            try
            {
                // 入力ファイルパス (.jpg)
                if (!options.TryGetValue("i", out string inputPath) || String.IsNullOrEmpty(inputPath))
                    throw new UsageException();
                // スライド定義ファイルパス (.json)
                if (!options.TryGetValue("t", out string transformPath) || String.IsNullOrEmpty(transformPath))
                    throw new UsageException();
                // 出力ファイルパス (.jpg)
                if (!options.TryGetValue("o", out string outputPath) || String.IsNullOrEmpty(outputPath))
                    throw new UsageException();

                // (オプション) 右トリミングサイズ (pixel)
                if (options.TryGetValue("tr", out string tr) && !Regex.IsMatch(tr, @"^[\d+]$", RegexOptions.Compiled))
                    throw new UsageException();
                // (オプション) 下トリミングサイズ (pixel)
                if (options.TryGetValue("tb", out string tb) && !Regex.IsMatch(tb, @"^[\d+]$", RegexOptions.Compiled))
                    throw new UsageException();

                int tr_ = Int32.TryParse(tr, out tr_) ? tr_ : 0;
                int tb_ = Int32.TryParse(tb, out tb_) ? tb_ : 0;

                JpegSlide(inputPath, transformPath, outputPath, tr_, tb_);
            }
            catch (UsageException)
            {
                ShowUsage();
                Environment.Exit(1);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"[{ex.GetType()}] {ex.Message}");
                Environment.Exit(1);
            }
		}

        private static void ShowUsage()
        {
            Console.WriteLine($@"
JPEG をロスレススライド変換します。

使い方: jpegslide -i [input.jpg] -t [transform.json] -o [output.jpg] -tr [trim_px] -tb [trim_px]

  -i    入力 JPEG ファイルのパス
  -t    スライド定義 JSON ファイルのパス
  -o    出力 JPEG ファイルのパス
  -tr   (オプション) 右トリミングサイズ (pixel)
  -tb   (オプション) 下トリミングサイズ (pixel)

");
        }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();
            var dllName = new AssemblyName(e.Name).Name + ".dll";

            var resources = thisAssembly.GetManifestResourceNames().Where(s => s.EndsWith(dllName));
            if (resources.Any())
            {
                var resourceName = resources.First();
                using (var stream = thisAssembly.GetManifestResourceStream(resourceName))
                {
                    if (stream == null)
                        return null;
                    try
                    {
                        var buffer = new byte[stream.Length];
                        stream.Read(buffer, 0, buffer.Length);
                        return Assembly.Load(buffer);
                    }
                    catch (System.IO.IOException)
                    {
                        return null;
                    }
                    catch (BadImageFormatException)
                    {
                        return null;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// 指定された JPEG 画像のブロックを JSON ファイルの指定に従ってスライド配置して保存します。(ロスレス変換)
        /// </summary>
        /// <param name="inputPath">入力 JPEG ファイルのパス</param>
        /// <param name="transformPath">JPEG スライド情報 JSON ファイルのパス</param>
        /// <param name="outputPath">出力先 JPEG ファイルのパス</param>
        /// <param name="trim_right">右側トリミングサイズ (pixel)</param>
        /// <param name="trim_bottom">下側トリミングサイズ (pixel)</param>
        /// <returns>成功した場合は true, それ以外は false</returns>
        public static void JpegSlide(string inputPath, string transformPath,  string outputPath, int trim_right = 0, int trim_bottom = 0)
        {
            try
            {
                var input = new jpeg_decompress_struct();
                using (var fileStream = new FileStream(inputPath, FileMode.Open))
                {
                    input.jpeg_stdio_src(fileStream);
                    input.jpeg_read_header(false);
                    var coefficients = input.jpeg_read_coefficients();

                    var component_count = coefficients.TakeWhile(c => c != null).Count();

                    for (var component = 0; component < component_count; component++)
                    {
                        var MCU_sample_width = input.Comp_info[component].MCU_sample_width();   // MCU サンプル幅/高さ (通常 8)
                        var width_in_blocks = input.Comp_info[component].Width_in_blocks;       // 水平方向のブロック数 (通常 画素幅 / 8)
                        var height_in_blocks = input.Comp_info[component].Height_in_blocks();   // 垂直方向のブロック数。LibJpeg は jpeg_component_info.Height_in_blocks がないのでリフレクションでアクセス

                        var block_width = (int)((input.Image_width + MCU_sample_width - 1) / width_in_blocks);     // 1ブロックに対応する幅 (px)
                        var block_height = (int)((input.Image_height + MCU_sample_width - 1) / height_in_blocks);  // 1ブロックに対応する高さ (px)

                        var transforms = (from t in JsonConvert.DeserializeObject<SlideTransform[]>(File.ReadAllText(transformPath))
                                          orderby t.sy, t.sx
                                          select t.ToBlock(block_width, block_height)).ToArray();

                        var source_blocks = coefficients[component].CloneArray();
                        foreach (var t in transforms)
                        {
                            for (var y = 0; y < t.sh; y++)
                            {
                                for (var x = 0; x < t.sw; x++)
                                {
                                    coefficients[component].Access(t.dy + y, 1)[0][t.dx + x] = source_blocks[t.sy + y][t.sx + x];
                                }
                            }
                        }
                    }

                    var output = new jpeg_compress_struct();
                    Directory.CreateDirectory(Path.GetDirectoryName(outputPath));

                    using (var outfile = new FileStream(outputPath, FileMode.Create))
                    {
                        output.jpeg_stdio_dest(outfile);

                        // jpeg_copy_critical_parameters() で input の画像サイズ (Output_width, Output_height) が output の SOF セグメントに書き込まれる前に変更してやるハック
                        if (0 < trim_right && trim_right < input.Output_width)
                        {
                            input.SetOutputWidth(input.Output_width - trim_right);
                        }
                        if (0 < trim_bottom && trim_bottom < input.Output_height)
                        {
                            input.SetOutputHeight(input.Output_height - trim_bottom);
                        }

                        input.jpeg_copy_critical_parameters(output);

                        output.jpeg_write_coefficients(coefficients);
                        output.jpeg_finish_compress();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 実行中のプロセスのコマンドラインからコマンドラインオプションをパースします。
        /// <para>キー: オプションスイッチ (例: "-s" の場合 "s"), 値: オプション値 (例: "-s value1" の場合 "value1") の辞書を返します。</para>
        /// </summary>
        /// <returns>コマンドラインオプション辞書</returns>
        public static Dictionary<string, string> ParseCommandLine()
        {
            var args = Environment.GetCommandLineArgs().Skip(1);
            var result = new Dictionary<string, string>();

            string option_swicth = null;

            foreach (var arg in args)
            {
                if (arg[0] == '-' || arg[0] == '/')
                {
                    option_swicth = arg.Substring(1);
                    result[option_swicth] = String.Empty;
                }
                else if (!String.IsNullOrEmpty(option_swicth))
                {
                    result[option_swicth] = arg;
                }
                else
                {
                    option_swicth = null;
                }
            }
            return result;
        }
    }


    #region SlideTransform

    public enum Unit
    {
        /// <summary>
        /// ピクセル単位
        /// </summary>
        Pixel,
        /// <summary>
        /// ブロック単位
        /// </summary>
        Block,
    }

    /// <summary>
    /// ブロックのスライド変換情報
    /// </summary>
    [DebuggerDisplay("unit = {unit}, (sx: {sx}, sy: {sy}, sw: {sw}, sh: {sh}) -> (dx: {dx}, dy: {dy})")]
    public class SlideTransform
    {
        /// <summary>
        /// 単位
        /// </summary>
        public Unit unit = Unit.Pixel;
        /// <summary>
        /// コピー元 X 位置 (0 origin)
        /// </summary>
        public int sx;
        /// <summary>
        /// コピー元 Y 位置 (0 origin)
        /// </summary>
        public int sy;
        /// <summary>
        /// コピー先 X 位置 (0 origin)
        /// </summary>
        public int dx;
        /// <summary>
        /// コピー先 Y 位置 (0 origin)
        /// </summary>
        public int dy;
        /// <summary>
        /// 幅 (pixel/ブロック数)
        /// </summary>
        public int sw;
        /// <summary>
        /// 高さ (pixel/ブロック数)
        /// </summary>
        public int sh;


        /// <summary>
        /// 単位を Pixel から Block に変換した SlideTransform を作成します。
        /// <para>ピクセル位置がブロックサイズの倍数でない場合は例外をスローします。</para>
        /// <para>幅/高さはブロックサイズ単位に拡張します。</para>
        /// </summary>
        /// <param name="block_width">ブロックあたりの幅 (単位: pixel)</param>
        /// <param name="block_height">ブロックあたりの高さ (単位: pixel)</param>
        /// <returns></returns>
        public SlideTransform ToBlock(int block_width, int block_height)
        {
            if (unit != Unit.Pixel)
                throw new InvalidOperationException($"unit が {Unit.Pixel} の場合のみ実行できます。");

            if (block_width <= 0)
                throw new ArgumentOutOfRangeException(nameof(block_width));
            if (block_height <= 0)
                throw new ArgumentOutOfRangeException(nameof(block_height));
            if (sx < 0)
                throw new ArgumentOutOfRangeException(nameof(sx));
            if (sy < 0)
                throw new ArgumentOutOfRangeException(nameof(sy));
            if (sw <= 0)
                throw new ArgumentOutOfRangeException(nameof(sw));
            if (sh <= 0)
                throw new ArgumentOutOfRangeException(nameof(sh));
            if (dx < 0)
                throw new ArgumentOutOfRangeException(nameof(dx));
            if (dy < 0)
                throw new ArgumentOutOfRangeException(nameof(dy));

            if (sx % block_width != 0)
                throw new InvalidOperationException($"{nameof(sx)} が {block_width} の倍数ではありません。");
            if (sy % block_height != 0)
                throw new InvalidOperationException($"{nameof(sy)} が {block_height} の倍数ではありません。");
            if (dx % block_width != 0)
                throw new InvalidOperationException($"{nameof(dx)} が {block_width} の倍数ではありません。");
            if (dy % block_height != 0)
                throw new InvalidOperationException($"{nameof(dy)} が {block_height} の倍数ではありません。");

            // 幅/高さをブロックサイズ単位にする。
            sw = ((sw + block_width - 1) / block_width) * block_width;
            sh = ((sh + block_height - 1) / block_height) * block_height;

            var range = new SlideTransform()
            {
                unit = Unit.Block,
                sx = sx / block_width,
                sy = sy / block_height,
                dx = dx / block_width,
                dy = dy / block_height,
                sw = sw / block_width,
                sh = sh / block_height,
            };
            return range;
        }
    }

    #endregion

}
